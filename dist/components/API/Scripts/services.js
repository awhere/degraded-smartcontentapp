﻿var testServices = angular.module('testServices', ['ngResource', 'appConfig']);

testServices.factory('Models', ['$http', 'MODELING_API_URL',

    function ($http, modelingUrl) {
        return {
            getModels: function (latitude, longitude, date, modelName, minTemp, maxTemp, optTemp, minWetHours, maxDryHours, utcOffset) {
                if (modelName) {
                    return $http({
                        method: 'GET',
                        url: modelingUrl + "/" + modelName + '?latitude=' + latitude + '&longitude=' + longitude + '&date=' + date + '&minTemperature=' + minTemp + '&maxTemperature=' + maxTemp + '&optimumTemperature=' + optTemp + '&minWetHours=' + minWetHours + '&maxDryHours=' + maxDryHours + '&utcOffset=' + utcOffset
                    });
                } else {
                    return $http({
                        method: 'GET',
                        url: modelingUrl + '?latitude=' + latitude + '&longitude=' + longitude + '&date=' + date + '&minTemperature=' + minTemp + '&maxTemperature=' + maxTemp + '&optimumTemperature=' + optTemp + '&minWetHours=' + minWetHours + '&maxDryHours=' + maxDryHours + '&utcOffset=' + utcOffset
                    });
                }
            },

            postModels: function (requests) {
                return $http({method: 'POST', url: modelingUrl, data: requests});
            }
        }
    }]);

testServices.factory('Login', ['$http', 'AUTH_URL',

    function ($http, authUrl) {
        return {
            authenticate: function (username, password) {
                $http.defaults.headers.common.Authorization = 'BASIC ' + btoa(username + ":" + password);

                return $http({method: 'GET', url: authUrl});
            }
        }
    }]);