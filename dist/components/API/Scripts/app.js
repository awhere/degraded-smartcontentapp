﻿var testApp = angular.module('testApp', [
  'ngRoute', 'testControllers', 'testServices'
]);

testApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider

        .when('/', {
              templateUrl: 'Content/partials/test1.html',
              controller: 'testCtrl'
        })

        .when('/test2', {
            templateUrl: 'Content/partials/test2.html',
            controller: 'testCtrl'
        })

        .otherwise({
            redirectTo: '/'
        });

  }]);