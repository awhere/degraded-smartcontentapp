﻿var testControllers = angular.module('testControllers', []);

testControllers.controller('testCtrl', ['$scope', 'Login', 'Models', function ($scope, Login, Models) {
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    $scope.requests =
        [

            {
                latitude: 41.672,
                longitude: -91.347,
                date: tomorrow,
                modelName: 'cornrust',
                utcOffset: '-4:00:00'
            },

            {
                latitude: 10.477,
                longitude: -69.296,
                date: tomorrow,
                minTemperature: 10,
                maxTemperature: 30,
                optimumTemperature: 20,
                minWetHours: 4,
                maxDryHours: 1,
                utcOffset: '-4:00:00'
            }
        ];

    $scope.submitLogin = function (username, password) {
        Login.authenticate(username, password)

            .success(function (data, status, headers, config) {
                $scope.isAuthenticated = true;
                $scope.authenticationError = false;
                $scope.asJSON = false;
                $scope.date = new Date().toJSON().slice(0, 10);
            }).

            error(function (data, status, headers, config) {
                $scope.isAuthenticated = false;
                $scope.authenticationError = true;
                console.log('error area');
            });
    };

    $scope.submitModelsRequest = function (latitude, longitude, date, modelName, minTemp, maxTemp, optTemp, minWetHours, maxDryHours, utcOffset) {
        Models.getModels(latitude, longitude, date, modelName, minTemp, maxTemp, optTemp, minWetHours, maxDryHours, utcOffset)

            .success(function (data, status, headers, config) {
                $scope.requestError = false;
                $scope.model = data;
                $scope.model.json = '\n' + angular.toJson(data, true);
            }).

            error(function (data, status, headers, config) {
                $scope.requestError = true;
                $scope.model = data;
                $scope.model.json = '\n' + angular.toJson(data, true);
            });
    };

    $scope.submitModelsPostRequest = function (requests) {
        Models.postModels(requests)

            .success(function (data, status, headers, config) {
                console.log(data);
                alert(data);
            }).

             error(function (data, status, headers, config) {
                 console.log(data);
                 alert(data);
             });
    };
}]);