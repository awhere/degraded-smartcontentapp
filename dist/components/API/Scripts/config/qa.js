﻿var appConfig = angular.module('appConfig', [])
    .constant('AUTH_URL', 'https://dataqa.awhere.com/api/authentication/login')
    .constant('MODELING_API_URL', 'https://dataqa.awhere.com/api/models/crop')
;