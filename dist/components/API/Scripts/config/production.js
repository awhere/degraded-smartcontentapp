﻿var appConfig = angular.module('appConfig', [])
    .constant('AUTH_URL', 'https://data.awhere.com/api/authentication/login')
    .constant('MODELING_API_URL', 'https://data.awhere.com/api/models/crop')
;