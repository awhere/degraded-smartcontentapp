/**
 * Created by
 * User: iankleinfeld
 * Date: 10/20/2014
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */

app.controller( "forecastTestController", function ( $scope ) {

    var svg = d3.select( "#forecast-test-icon" ).append( "svg" )
        .attr( {
                   "width"  : "150",
                   "height" : "100",
                   "viewBox": "0 0 150 100",
                   "id"     : "day-one"
               } );

    svg.append( "rect" )
        .attr( {
                   "width" : "100%",
                   "height": "100%",
                   "class" : "forecast-background"

               } );

    var iconSpace = d3.select( "#day-one" ).append( "svg:g" ).select( function () {
        var mySun = d3.select( "#sun" ).attr({ "id": null, "class":"sun" }).node();
        return this.appendChild( mySun );
    } );



} );
