/**
 * Created by iankleinfeld on 10/10/2014.
 */
//  ########  #######  ########  ########  ######     ###     ######  ########
//  ##       ##     ## ##     ## ##       ##    ##   ## ##   ##    ##    ##
//  ##       ##     ## ##     ## ##       ##        ##   ##  ##          ##
//  ######   ##     ## ########  ######   ##       ##     ##  ######     ##
//  ##       ##     ## ##   ##   ##       ##       #########       ##    ##
//  ##       ##     ## ##    ##  ##       ##    ## ##     ## ##    ##    ##
//  ##        #######  ##     ## ########  ######  ##     ##  ######     ##
//
//   ######   #######  ##    ## ######## ########   #######  ##       ##       ######## ########
//  ##    ## ##     ## ###   ##    ##    ##     ## ##     ## ##       ##       ##       ##     ##
//  ##       ##     ## ####  ##    ##    ##     ## ##     ## ##       ##       ##       ##     ##
//  ##       ##     ## ## ## ##    ##    ########  ##     ## ##       ##       ######   ########
//  ##       ##     ## ##  ####    ##    ##   ##   ##     ## ##       ##       ##       ##   ##
//  ##    ## ##     ## ##   ###    ##    ##    ##  ##     ## ##       ##       ##       ##    ##
//   ######   #######  ##    ##    ##    ##     ##  #######  ######## ######## ######## ##     ##


app.controller( "forecastController", function ( $scope, $http ) {
    $scope.forecastData = [];
    $http.get( 'js/data/multiPointForecast.json' ).success( function ( data ) {
        $scope.forecastData = data;

//  ########     ###    ######## ########  ######
//  ##     ##   ## ##      ##    ##       ##    ##
//  ##     ##  ##   ##     ##    ##       ##
//  ##     ## ##     ##    ##    ######    ######
//  ##     ## #########    ##    ##             ##
//  ##     ## ##     ##    ##    ##       ##    ##
//  ########  ##     ##    ##    ########  ######

        function gmtFix( someDate ) {
            return new Date( someDate.replace( /T00/, 'T05' ) );
        }

        $scope.timeOfDay = function ( i ) {
            var daytimes = ["Early", "Morning", "Afternoon", "Night"];
            //var daytimes = ["00:00 - 06:00", "06:00 - 12:00", "12:00 - 18:00 ", "18:00 - 23:59"];
            return daytimes[i];
        };

        $scope.whatDayNum = function ( i, theDate ) {
            //  console.log( 'raw date', theDate.replace(/T00/,'T05') );
            // TODO this needs to be adjusted for GMT of the displaying locale!!
            // FIXME before production, currently hacked by adding 5 hours to the date
            var slug,
                formattedDate = gmtFix( theDate );

            if ( i == 0 ) {
                slug = 'Today';
            } else {
                slug = '+' + i;
            }

            slug = slug + ' | ' + formattedDate.getDate() + ' ' + app.fx.months( formattedDate );
            return slug;
        };

        $scope.whatDayName = function ( theDate ) {
            var formattedDate = new Date( theDate );
            return app.fx.days( formattedDate );

        };

        $scope.myID = function ( i, parenti ) {
            return tempID = 'day' + parenti + '-tod' + i;
        };

//  ####  ######   #######  ##    ##  ######
//   ##  ##    ## ##     ## ###   ## ##    ##
//   ##  ##       ##     ## ####  ## ##
//   ##  ##       ##     ## ## ## ##  ######
//   ##  ##       ##     ## ##  ####       ##
//   ##  ##    ## ##     ## ##   ### ##    ##
//  ####  ######   #######  ##    ##  ######



        $scope.iconGenerator = function ( i, parenti, condCode ) {

            var thisBox = $scope.myID( i, parenti );

            var thisID = '#' + thisBox;

            var timeOfDayClass, isNight = true;

            var wCodes = {
                "cloudy": condCode.substr( 0, 1 ),
                "rainy" : condCode.substr( 1, 1 ),
                "windy" : condCode.substr( 2, 1 )
            };

            switch ( i ) {
                case 0:
                    timeOfDayClass = " early";
                    break;
                case 1:
                    timeOfDayClass = " morning";
                    isNight = false;
                    break;
                case 2:
                    timeOfDayClass = " afternoon";
                    isNight = false;
                    break;
                case 3:
                    timeOfDayClass = " night";
                    break;
                default:
                    timeOfDayClass = " error";
                    break;
            }


            var svg = d3.select( thisID ).append( "svg" )
                .attr( "width", "150" )
                .attr( "height", "100" )
                .attr("class", "outline"+timeOfDayClass)
                .attr( "viewBox", "0 0 150 100" );

            svg.append( "rect" )
                .attr( "width", "100%" )
                .attr( "height", "100%" )
                .attr( "class", function () {
                           return "forecast-background" + timeOfDayClass;
                       } );
            
            isNight === false ? sunMaker( svg ) : moonMaker( svg );

            cloudMaker( svg, wCodes );
        };


//                      ____ _                 _
//     ___  _ __   ___ / ___| | ___  _   _  __| |
//    / _ \| '_ \ / _ \ |   | |/ _ \| | | |/ _` |
//   | (_) | | | |  __/ |___| | (_) | |_| | (_| |
//    \___/|_| |_|\___|\____|_|\___/ \__,_|\__,_|
//

        function oneCloud( svg, stormClass, cloudInfo, m ) {

            var myX, myY;

            if (cloudInfo.quantity > 1 ){
                if ( m > 1 ) {
                    myX = cloudInfo.coords[cloudInfo.proximity].x1;
                    myY = cloudInfo.coords[cloudInfo.proximity].y1;
                } else {
                    myX = cloudInfo.coords[cloudInfo.proximity].x;
                    myY = cloudInfo.coords[cloudInfo.proximity].y;
                }
            } else {
                myX = cloudInfo.x, myY = cloudInfo.y;
            }

            svg.append( "g" )
                .attr( "transform", app.fx.svg.translateMe( myX, myY ) )
                .attr( "class", "clouds" )
                .append( "path" )
                .attr( "class", "cloud filled-icon "+ stormClass )
                .attr( "d", "M104.4,41.4c-1.4,0-2.8,0.2-4.1,0.5c-2.5-9.2-11.4-16-21.9-16c-7.6,0-14.3,3.5-18.4,9c-2.3-0.8-4.7-1.2-7.3-1.2c-12,0-21.7,9.2-21.7,20.5c0,11.3,9.7,20.5,21.7,20.5h51.3v0c0.1,0,0.2,0,0.4,0c9.2,0,16.6-7.4,16.6-16.6C121,48.9,113.6,41.4,104.4,41.4z" );

            //svg.select("g").append("text" )
            //    .text('p'+cloudInfo.proximity + ' myX: '+myX)
            //    .attr("fill","black")
            //    .attr(  "transform","translate("+8+","+(8+(5*(m*m)))+")");
        }


//              _       __  __       _
//    _ __ __ _(_)_ __ |  \/  | __ _| | _____ _ __
//   | '__/ _` | | '_ \| |\/| |/ _` | |/ / _ \ '__|
//   | | | (_| | | | | | |  | | (_| |   <  __/ |
//   |_|  \__,_|_|_| |_|_|  |_|\__,_|_|\_\___|_|
//
        function rainMaker( svg, rainLoops ) {
            for ( var n = 0; n < rainLoops; n++ ) {
                svg.select( ".ray-5" ).remove();
                svg.selectAll( ".clouds" )
                    .append( "path" )
                    .attr( "class", "raindrop filled-icon" )
                    .attr( "d", "M49.1,86.7c0.3-1.7-0.3-2.9-0.8-7.4c-2,3.9-3.2,4.9-3.5,6.6c-0.3,1.7,0.4,3.2,1.6,3.4S48.8,88.4,49.1,86.7z" )
                    .attr( "transform", function () {
                               var cloud = this.parentNode.getBBox();
                               var drops = {};
                               drops.wid = Math.round( cloud.width / (rainLoops + 2) );
                               drops.ran = drops.wid * rainLoops;
                               drops.pad = Math.round( (cloud.width - drops.ran) / 2 ) - (rainLoops * 1.75);
                               drops.x = drops.pad + (n * drops.wid);
                               drops.y = n % 2 === 0 ? 0 : 4;

                               return app.fx.svg.translateMe( drops.x, drops.y );
                           } );
            }
        }



//         _                 _ __  __       _
//     ___| | ___  _   _  __| |  \/  | __ _| | _____ _ __
//    / __| |/ _ \| | | |/ _` | |\/| |/ _` | |/ / _ \ '__|
//   | (__| | (_) | |_| | (_| | |  | | (_| |   <  __/ |
//    \___|_|\___/ \__,_|\__,_|_|  |_|\__,_|_|\_\___|_|
//
        function cloudMaker( svg, wCodes ) {

            var rainLoops = 2 * ( wCodes.rainy - 1); // allows us to skip rain loop completely
            var stormClass;
            var cloudInfo = {
                "x"        : 10,
                "y"        : -5,
                "quantity" : 1,
                "proximity": 0,
                "coords"   : [
                    {
                        "x" : 50,
                        "y" : -5,
                        "x1": -55,
                        "y1": 0
                    },
                    {
                        "x" : 30,
                        "y" : -5,
                        "x1": -40,
                        "y1": 0
                    }
                ]
            };

            // cloud position and quantity
            switch ( wCodes.cloudy ) {
                case "2":
                case "7":
                    // 12.5% to 37.5% opaque
                    wCodes.cloudy = 2;
                    cloudInfo.x = 55;
                    break;

                case "3":
                case "8":
                    // 37.5% to 62.5% opaque
                    wCodes.cloudy = 3;
                    break;

                case "4":
                case "9":
                    // 62.5% to 87.5% opaque
                    wCodes.cloudy = 4;
                    cloudInfo.quantity = 2;
                    cloudInfo.proximity = 0;
                    break;

                case "5":
                case "A":
                    // 87.5% - 100% opaque
                    wCodes.cloudy = 5;
                    cloudInfo.quantity = 2;
                    cloudInfo.proximity = 1;
                    break;

                default:
                    // sunny
                    return;
                    break;
            }

            // cloud color
            switch ( rainLoops ) {
                case 0:
                    stormClass = "fair";
                    break;
                case 2:
                    stormClass = "unfair";
                    break;
                case 4:
                    stormClass = "storm";
                    break;
                case 6:
                    stormClass = "heavy-storm";
                    break;
                default:
                    break;
            }

            // add cloud icon(s)
            for ( var m = 1; m <= cloudInfo.quantity; m++ ) {
                oneCloud( svg, stormClass, cloudInfo, m );
            }

            // add raindrops
            rainMaker( svg, rainLoops );
        }

//                    __  __       _
//    ___ _   _ _ __ |  \/  | __ _| | _____ _ __
//   / __| | | | '_ \| |\/| |/ _` | |/ / _ \ '__|
//   \__ \ |_| | | | | |  | | (_| |   <  __/ |
//   |___/\__,_|_| |_|_|  |_|\__,_|_|\_\___|_|
//

        function sunMaker( svg ) {
            var ray = ["M38.5,6.6l2.1,8.3c0.1,0.5-0.2,1.1-0.7,1.2l0,0c-0.5,0.1-1.1-0.2-1.2-0.7l-2.1-8.3c-0.1-0.5,0.2-1.1,0.7-1.2l0,0C37.8,5.7,38.4,6,38.5,6.6z",
                       "M60.1,17.7l1.8-3c0.3-0.5,0.9-0.6,1.4-0.4l0,0c0.5,0.3,0.6,0.9,0.4,1.4l-1.8,3c-0.3,0.5-0.9,0.6-1.4,0.4l0,0C60,18.9,59.8,18.2,60.1,17.7z",
                       "M82.6,34.4l-8.3,2.1c-0.5,0.1-1.1-0.2-1.2-0.7v0c-0.1-0.5,0.2-1.1,0.7-1.2l8.3-2.1c0.5-0.1,1.1,0.2,1.2,0.7l0,0C83.5,33.7,83.1,34.2,82.6,34.4z",
                       "M71.9,56.1l3,1.8c0.5,0.3,0.6,0.9,0.4,1.4l0,0c-0.3,0.5-0.9,0.6-1.4,0.4l-3-1.8c-0.5-0.3-0.6-0.9-0.4-1.4v0C70.8,55.9,71.4,55.8,71.9,56.1z",
                       "M54.9,78.6l-2.1-8.3c-0.1-0.5,0.2-1.1,0.7-1.2l0,0c0.5-0.1,1.1,0.2,1.2,0.7l2.1,8.3c0.1,0.5-0.2,1.1-0.7,1.2h0C55.6,79.5,55,79.2,54.9,78.6z",
                       "M33.2,67.5l-1.8,3c-0.3,0.5-0.9,0.6-1.4,0.4h0c-0.5-0.3-0.6-0.9-0.4-1.4l1.8-3c0.3-0.5,0.9-0.6,1.4-0.4l0,0C33.4,66.4,33.5,67,33.2,67.5z",
                       "M10.8,50.9l8.3-2.1c0.5-0.1,1.1,0.2,1.2,0.7l0,0c0.1,0.5-0.2,1.1-0.7,1.2l-8.3,2.1c-0.5,0.1-1.1-0.2-1.2-0.7v0C9.9,51.6,10.2,51,10.8,50.9z",
                       "M21.5,29.2l-3-1.8c-0.5-0.3-0.6-0.9-0.4-1.4l0,0c0.3-0.5,0.9-0.6,1.4-0.4l3,1.8c0.5,0.3,0.6,0.9,0.4,1.4v0C22.6,29.3,22,29.4,21.5,29.2z"];

            svg.append( "g" )
                .attr( "class", "sun filled-icon" )
                .append( "circle" )
                .attr( "cx", "46.7" )
                .attr( "cy", "42.5" )
                .attr( "r", "24" );

            for ( var n = 0; n <= ray.length - 1; n++ ) {
                svg.select( ".sun" )
                    .append( "path" )
                    .attr( "d", ray[n] )
                    .attr( "class", "ray ray-" + (n + 1) );
            }
        }

//                                __  __       _
//    _ __ ___   ___   ___  _ __ |  \/  | __ _| | _____ _ __
//   | '_ ` _ \ / _ \ / _ \| '_ \| |\/| |/ _` | |/ / _ \ '__|
//   | | | | | | (_) | (_) | | | | |  | | (_| |   <  __/ |
//   |_| |_| |_|\___/ \___/|_| |_|_|  |_|\__,_|_|\_\___|_|
//
        function moonMaker( svg ) {

            svg.append( "g" )
                .attr( "class", "night-sky" )
                .append( "path" )
                .attr( "class", "moon filled-icon" )
                .attr( "d", "M49,25.2C46.7,16.4,39.8,10,31.5,7.9c3.3,3,5.8,7,7,11.7c3.3,12.9-4.4,26-17.3,29.3c-4,1-8,1-11.8,0c5.8,5.3,14.1,7.7,22.3,5.6C44.5,51.2,52.3,38.1,49,25.2z" );

            var starCoords = [
                "-60,-40", "0,-25", "55,-40", "45,-25", "30,-35", "58,15", "10,0", "-36,35", "-50,20"
            ];
            for ( var n = 0; n <= starCoords.length - 1; n++ ) {

                // x range -60 (left) to +60 (right)
                // y range -40 (top) to +40 (bottom)

                var thisStar = starCoords[n].split( "," );

                svg.select( ".night-sky" )
                    .append( "polygon" )
                    .attr( "points", "73.8,49.5 74.1,48.4 74.8,49.3 75.9,49.3 75.3,50.2 75.6,51.3 74.5,50.9 73.6,51.6 73.6,50.5 72.7,49.8" )
                    .attr( "class", "star filled-icon star-" + n )
                    .attr( "transform", app.fx.svg.translateMe( thisStar[0], thisStar[1] ) );
            }
        }


    } );
} )
;


//  ##     ## ####  ######  ########  #######  ########  ####  ######
//  ##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##    ##
//  ##     ##  ##  ##          ##    ##     ## ##     ##  ##  ##
//  #########  ##   ######     ##    ##     ## ########   ##  ##
//  ##     ##  ##        ##    ##    ##     ## ##   ##    ##  ##
//  ##     ##  ##  ##    ##    ##    ##     ## ##    ##   ##  ##    ##
//  ##     ## ####  ######     ##     #######  ##     ## ####  ######

app.controller( "historicController", function ( $scope, $http ) {

    $scope.historicData = [];
    $http.get( 'js/data/historicData.json' ).success( function ( data ) {
        $scope.historicData = data;
        $scope.predicate = 'date';


        $scope.shortDate = function ( theDate ) {
            var formattedDate = new Date( theDate.replace( /T00/, 'T05' ) );
            return formattedDate.getDate() + ' ' + app.fx.months( formattedDate );
        };


    } );
} );
