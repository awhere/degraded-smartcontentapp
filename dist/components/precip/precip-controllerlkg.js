app.controller("precipController", function($scope, $http) {
    $scope.precipData = [];
    $http.get('js/data/precipdata.json').success(function(data) {
        //console.log("success!");
        $scope.precipData = data;



        var width = $("#precipitation-panel").width(),
            height = 500,
            myPadding = 42;

// set linear scale
        var y = d3.scale.linear()
            .range([height, 0]);

// where to put the chart?
        var chart = d3.select("#precip-chart")
            .attr("width", width)
            .attr("height", height);


            // determine the max value of the y-axis
            var ymax = d3.max(data, function (d) {
                return parseInt(d.amount);
            });

            // chart horizontal rules

            var barTop = ymax - 20,
                ruleRange = Math.floor((height - barTop) / 4),
                ruleLocations = [ymax - 20, ruleRange + barTop, ruleRange * 2 + barTop, ruleRange * 3 + barTop, height - 1];

            for (i = 0; i <= 4; i++) {
                chart.append("line").attr("x1", 0).attr("y1", ruleLocations[i]).attr("x2", width).attr("y2", ruleLocations[i]).attr("class", "x-rule");
                //   chart.append("text").attr("x",30).attr("y",ruleLocations[i]).attr("class","x-legend").text(ruleLocations[i]);
                //  console.log(ruleRange+ ' | '+ i+' | '+ ruleLocations[i]);
            }


            // I add 10% to give headroom for the y domain
            y.domain([0, Math.floor(ymax * 1.1)]);

            // set width of bar to same width as weather icon 83 is the total padding, margin, and border for each plus the panel padding
            var barWidth = Math.ceil((width - (83 * 5)) / 5);

            // set spacing between the bars,
            var barSpacing = (width / data.length);

            // set the heights of the bars d is for data item
            var bar = chart.selectAll("g")
                .data(data)
                .enter().append("g")
                .attr("transform", function (d, i) {
                    return "translate(" + parseInt(myPadding + (i * barSpacing)) + ",0)";
                });

            // add the bars

            // create wavy top columns
            bar.append("path")
                .attr("class", function (d) {
                    return "water-level chance-" + (Math.ceil(d.chance / 5) * 5);
                })
                .attr("d", function (d) {
                    var barHeight = Math.floor(height - y(d.amount)),
                        myY = height - barHeight;
                    return 'M0 ' + myY + ' Q ' + Math.floor(barWidth * 0.25) + ' ' + Math.floor(myY + 10) + ', ' + Math.floor(barWidth * 0.5) + ' ' + myY + ' T ' + barWidth + ' ' + myY + ' v ' + barHeight + ' h -'+barWidth+ ' v -' + barHeight + 'z';
                    //return "M0 10 Q 35 20, 70 10 T 140 10 v  "+ barHeight+" h -140 v -"+barHeight+"z"
                });

            // add the text

            // AMOUNT text at top of bar
            bar.append("text")
                .attr("x", barWidth)
                .attr("y", function (d) {
                    return y(d.amount) - 10;
                })
                .attr("class", function (d) {
                    return "rainfall-amt chance-" + (Math.ceil(d.chance / 5) * 5);
                })
                .text(function (d) {
                    return d.amount + 'mm';
                });

            // PROBABILITY text at base of bar
            bar.append("text")
                .attr("x", barWidth - 5)
                .attr("y", 495)
                .attr("class", function (d) {
                    return "rainfall-chance chance-" + (Math.ceil(d.chance / 5) * 5);
                })
                .text(function (d) {
                    return d.chance + '%';
                });


        function type(d) {
            d.value = +d.value; // coerce to number
            return d;
        }
//  <path d="M0 10 Q 35 20, 70 10 T 140 10 v 10 h -140 v -10" />





    });
});



