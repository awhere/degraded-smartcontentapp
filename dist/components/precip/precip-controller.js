//  ########  ########  ########  ######  #### ########  #### ########    ###    ######## ####  #######  ##    ##
//  ##     ## ##     ## ##       ##    ##  ##  ##     ##  ##     ##      ## ##      ##     ##  ##     ## ###   ##
//  ##     ## ##     ## ##       ##        ##  ##     ##  ##     ##     ##   ##     ##     ##  ##     ## ####  ##
//  ########  ########  ######   ##        ##  ########   ##     ##    ##     ##    ##     ##  ##     ## ## ## ##
//  ##        ##   ##   ##       ##        ##  ##         ##     ##    #########    ##     ##  ##     ## ##  ####
//  ##        ##    ##  ##       ##    ##  ##  ##         ##     ##    ##     ##    ##     ##  ##     ## ##   ###
//  ##        ##     ## ########  ######  #### ##        ####    ##    ##     ##    ##    ####  #######  ##    ##
//
//   ######   #######  ##    ## ######## ########   #######  ##       ##       ######## ########
//  ##    ## ##     ## ###   ##    ##    ##     ## ##     ## ##       ##       ##       ##     ##
//  ##       ##     ## ####  ##    ##    ##     ## ##     ## ##       ##       ##       ##     ##
//  ##       ##     ## ## ## ##    ##    ########  ##     ## ##       ##       ######   ########
//  ##       ##     ## ##  ####    ##    ##   ##   ##     ## ##       ##       ##       ##   ##
//  ##    ## ##     ## ##   ###    ##    ##    ##  ##     ## ##       ##       ##       ##    ##
//   ######   #######  ##    ##    ##    ##     ##  #######  ######## ######## ######## ##     ##

app.controller( "precipController", function ( $scope, $http ) {
    $scope.precipData = [];
    $http.get( 'js/data/singlePointForecast.json' ).success( function ( data ) {

        // TODO remove for production just dealing with a 5+2 dataset for the temperature model
        data.splice( 0, 1 );
        data.splice( -1, 1 );

        $scope.precipData = data;


        var width = $( "#precipitation-panel" ).width(),
            height = 500,
            myPadding = 42,
            precipArray = [];

// set linear scale
//        var y = d3.scale.linear()
//            .range( [height, 0] );

// where to put the chart?
        var chart = d3.select( "#precip-chart" )
            .attr( "width", width )
            .attr( "height", height );

// create array for precip amounts
        for ( var i = 0; i <= data.length - 1; i++ ) {
            precipArray.push( data[i].conditions[0].precip );
        }

        // determine the max value of the y-axis
        var ymax = d3.max( precipArray, function ( d ) {
            return Math.ceil( d );
        } );

        var y = d3.scale.linear()
            .range( [height, 0] );

        //chart horizontal rules

        var barTop = ymax - 20,
            ruleRange = Math.floor( (height - barTop) / 5 ),
            ruleLocations = [ymax - 20, ruleRange + barTop, ruleRange * 2 + barTop, ruleRange * 3 + barTop, ruleRange * 4 + barTop, height - 1];

        for ( var i = 0; i <= 6; i++ ) {
            chart.append( "line" ).attr( "x1", 0 ).attr( "y1", ruleLocations[i] ).attr( "x2", width ).attr( "y2", ruleLocations[i] ).attr( "class", "x-rule" );
        }

        // I add 10% to give headroom for the water curve in the y domain and make the range a minimum of 25mm
        var yDomainMax = Math.floor( ymax * 1.27 );

        yDomainMax = yDomainMax >= 25 ? yDomainMax : 25;
        y.domain( [0, yDomainMax] );

        // set width of bar to same width as weather icon 83 is the total padding, margin, and border for each plus the panel padding
        var barWidth = Math.ceil( (width - (83 * 5)) / 5 );

        // set spacing between the bars,
        var barSpacing = (width / data.length);

        // set the heights of the bars
        var bar = chart.selectAll( "tempg" )
            .data( data )
            .enter().append( "g" )
            .attr( "transform", function ( d, i ) {
                       return "translate(" + parseInt( myPadding + (i * barSpacing) ) + ",0)";
                   } );


        // add the bars

        // create wavy top columns
        bar.append( "path" )
            .attr( "class", function ( d ) {
                       return "water-level chance-" + (Math.ceil( d.conditions[0].precipPercent / 5 ) * 5);
                   } )
            .attr( "d", function ( d ) {
                       var barHeight = Math.floor( height - y( d.conditions[0].precip ) ),
                           myY = height - barHeight;
                       // console.log( 'myY:'+myY );
                       return 'M0 ' + myY + ' Q ' + Math.floor( barWidth * 0.25 ) + ' ' + Math.floor( myY + 10 ) + ', ' + Math.floor( barWidth * 0.5 ) + ' ' + myY + ' T ' + barWidth + ' ' + myY + ' v ' + barHeight + ' h -' + barWidth + ' v -' + barHeight + 'z';

                   } );

        // add the text

        // AMOUNT text at top of bar
        bar.append( "text" )
            .attr( "x", barWidth  )
            .attr( "y", function ( d ) {
                       return y( d.conditions[0].precip ) - 10;
                   } )
            .attr( "class", function ( d ) {
                       return "rainfall-amt chance-" + (Math.ceil( d.conditions[0].precipPercent / 5 ) * 5);
                   } )
            .text( function ( d ) {
                               var tinyPrecipText ="";
                               if ( Math.ceil( d.conditions[0].precip ) < 2 ) {
                                  tinyPrecipText = ' : ' + Math.ceil( d.conditions[0].precipPercent ) + '%';
                               }
                       return Math.ceil( d.conditions[0].precip ) + 'mm' + tinyPrecipText;
                   } );

        // PROBABILITY text at base of bar
        bar.append( "text" )
            .attr( "x", barWidth - 5 )
            .attr( "y", 495 )
            .attr( "class", function ( d ) {
                       var smallClass = "";
                       if ( Math.ceil( d.conditions[0].precip ) < 2 ) {
                           // account for 1mm rainfall that's not tall enough for the % number
                           smallClass = " tinyPrecip";
                       }
                       return "rainfall-chance chance-" + (Math.ceil( d.conditions[0].precipPercent / 5 ) * 5) + smallClass;
                   } )
            .text( function ( d ) {
                       return Math.ceil( d.conditions[0].precipPercent ) + '%';
                   } );


    } ); // end of data json
} );



