/**
 * Created by iankleinfeld on 10/2/2014.
 */

    var map = null;
    var query,
        bingKey = 'AsUgSBc-qyUIZyfdhzexL_B4k7YL6yDSOYVS3rg8tb_rbqHwnaHSaueyDPRSUxpR';

    function getMap() {
        map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
            credentials: bingKey,
            enableClickableLogo: false,
            enableSearchLogo: false,
            showMapTypeSelector: false,
            showBreadcrumb: true
        });

        map.setView({
            zoom: 6,
            center: new Microsoft.Maps.Location(app.location.latitude, app.location.longitude)
        });

    }

    function findLocation(whereTo) {
        var bbox = whereTo.bbox;
        var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(bbox[0], bbox[1]), new Microsoft.Maps.Location(bbox[2], bbox[3]));
        map.setView({bounds: viewBoundaries});
        var location = new Microsoft.Maps.Location(whereTo.geocodePoints[0].coordinates[0], whereTo.geocodePoints[0].coordinates[1]);
        var pushpin = new Microsoft.Maps.Pushpin(location);
        map.entities.push(pushpin);

        app.location.latitude = whereTo.geocodePoints[0].coordinates[0];
        app.location.longitude = whereTo.geocodePoints[0].coordinates[1];

        // get aWhere weather API data
        console.log(app.location);

    }


    $("#searchBox").autocomplete({
        source: function (request, response) {
            //  http://dev.virtualearth.net/REST/v1/Locations?query=" + encodeURI(document.getElementById('txtQuery').value) + "&output=json&jsonp=GeocodeCallback&key=" + credentials
            $.ajax({
                url: "http://dev.virtualearth.net/REST/v1/Locations",
                dataType: "jsonp",
                data: {
                    key: bingKey,
                    q: request.term
                },
                jsonp: "jsonp",
                success: function (data) {
                    var result = data.resourceSets[0],
                        comma1 = ", ",
                        comma2 = ", ",
                        pipe = " | ",
                        totsItems = 3;
                    if (result) {
                        if (result.estimatedTotal > 0) {
                            // console.log(result);
                            response($.map(result.resources, function (item) {
                                //console.log(item);
                                if (item.address.adminDistrict === undefined) {
                                    item.address.adminDistrict = "";
                                    comma1 = "";
                                    totsItems--;
                                }
                                if (item.address.adminDistrict2 === undefined) {
                                    item.address.adminDistrict2 = "";
                                    comma2 = "";
                                    totsItems--;
                                }
                                if (item.address.countryRegion === undefined || item.address.countryRegion === item.name) {
                                    item.address.countryRegion = "";
                                    totsItems--;
                                }
                                if (totsItems < 1) {
                                    pipe = " ";
                                }
                                if (item.address.locality === undefined) {
                                    item.address.locality = item.name.slice(0, item.name.indexOf(","));
                                }
                                return {
                                    data: item,
                                    label: item.address.locality + pipe + item.address.adminDistrict + comma1 + item.address.adminDistrict2 + comma2 + item.address.countryRegion,
                                    value: item.name


                                }
                            }));
                        }
                    }
                }
            });
        },
        minLength: 1,
        change: function (event, ui) {
            if (!ui.item)
                $("#searchBox").val('');
        },
        select: function (event, ui) {
            // here's where to make the API call for weather data
            findLocation(ui.item.data);
        }
    });

    getMap();
