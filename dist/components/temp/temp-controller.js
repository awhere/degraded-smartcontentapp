/**
 * Created by iankleinfeld on 10/10/2014.
 */
app.controller( "temperatureController", function ( $scope, $http ) {

    $scope.temperatureData = [];
    $http.get( 'js/data/singlePointForecast.json' ).success( function ( data ) {
        $scope.temperatureData = data;

        function parseDate( aDate ) {
            return aDate.substring( 0, 10 ).replace( /-/g, '' );
        };

        // set width of bar to same width as weather icon 83 is the total padding, margin, and
        // border for each plus the panel padding

        var barWidth = Math.ceil( ($( "#precipitation-panel" ).width() - (83 * 5)) / 5 );

        // set spacing between the bars,
        var barSpacing = ($( "#precipitation-panel" ).width() / 5);

        var paddingK = 45;


        var margin = {top: 0, right: 0, bottom: 0, left: 0},
            width = barSpacing * 7, // including today and +6
            height = 500,
            gridWidth = $( "#precipitation-panel" ).width();
        //console.log( 'barSpacing:', barSpacing, 'barWidth:', barWidth, 'width:', width );


        var x = d3.time.scale()
            .range( [0, width] );

        var y = d3.scale.linear()
            .range( [height - 20, 0] ); // the -20 is to give room for 10px padding within the svgs, show full scale

        var area = d3.svg.area()
            .x( function ( d, i ) {
                    return i * barSpacing;
                } )
            .y0( function ( d ) {
                     return y( d.conditions[0].minTemperature );
                 } )
            .y1( function ( d ) {
                     return y( d.conditions[0].maxTemperature );
                 } );

        var svgBG = d3.select( "#temperature-grid" ).append( "svg" )
            .attr( {
                       "width" : gridWidth,
                       "height": height
                   } );

        var svg = d3.select( "#temperature-chart" ).append( "svg" )
            .attr( {
                       "width" : gridWidth - (2 * paddingK) + margin.left - margin.right,
                       "height": height + margin.top + margin.bottom,
                       "id"    : "svg-myTemps"
                   } )
            .append( "g" )
            .attr( "transform", "translate(" + (-(barSpacing / 2) - paddingK) + "," + margin.top + ")" ); // add left margin +/-


        var maxLine = d3.svg.line()
            .x( function ( d, i ) {
                    return i * barSpacing;
                } )
            .y( function ( d ) {
                    return y( d.conditions[0].maxTemperature );
                } );

        var minLine = d3.svg.line()
            .x( function ( d, i ) {
                    return i * barSpacing;
                } )
            .y( function ( d ) {
                    return y( d.conditions[0].minTemperature );
                } );

        data.forEach( function ( d ) {
            d.date = parseDate( d.date );
            d.conditions[0].minTemperature = +d.conditions[0].minTemperature;
            d.conditions[0].maxTemperature = +d.conditions[0].maxTemperature;
        } );

        x.domain( d3.extent( data, function ( d ) {
            return d.date;
        } ) );

        var myTemps = {};
        myTemps.lo = d3.min( data, function ( d ) {
            return d.conditions[0].minTemperature;
        } );
        myTemps.hi = d3.max( data, function ( d ) {
            return d.conditions[0].maxTemperature;
        } );
        myTemps.top = Math.ceil( myTemps.hi + 5 );
        myTemps.bottom = Math.floor( myTemps.lo - 5 );
        myTemps.extRange = myTemps.top - myTemps.bottom;


        // y.domain( [0, 50] );
        y.domain( [myTemps.bottom, myTemps.top] );

        //chart horizontal rules

        var ruleLocations = [1, height * 0.2, height * .4, height * .6, height * .8, height - 1];

        for ( var i = 0; i <= 5; i++ ) {
            svgBG.append( "line" ).attr( "x1", 0 ).attr( "y1", ruleLocations[i] ).attr( "x2", width ).attr( "y2", ruleLocations[i] ).attr( "class", "x-rule" );
        }


        // background area / bivariate area
        svg.append( "path" )
            .datum( data )
            .attr( "class", "area" )
            .attr( "d", area );

        // max line
        svg.append( "path" )
            .datum( data )
            .attr( "class", "max-line" )
            .attr( "d", maxLine );

        // min-line
        svg.append( "path" )
            .datum( data )
            .attr( "class", "min-line" )
            .attr( "d", minLine );

        // deal with MIN and MAX dots an their text
        for ( var c = 0; c <= 1; c++ ) {
            var dotSet;
            c == 0 ? dotSet = "min" : dotSet = "max";

            // Add the data point dots
            svg.selectAll( "dot" )
                .data( data )
                .enter().append( "circle" )
                .attr( "class", "myTemps-dot" + ' ' + dotSet )
                .attr( "r", 5 )
                .attr( "cx", function ( d, i ) {
                           return i * barSpacing;
                       } )
                .attr( "cy", function ( d ) {
                           if ( c == 0 ) {
                               return y( d.conditions[0].minTemperature );
                           } else {
                               return y( d.conditions[0].maxTemperature );
                           }
                       } );


            svg.selectAll( "ttext" )
                .data( data )
                .enter().append( "text" )
                .attr( "class", "dot-text" + ' ' + dotSet )
                .attr( "x", function ( d, i ) {
                           return i * barSpacing;
                       } )
                .attr( "y", function ( d ) {
                           if ( c == 0 ) {
                               return y( d.conditions[0].minTemperature );
                           } else {
                               return y( d.conditions[0].maxTemperature );
                           }
                       } )
                .attr( "dy", function () {
                           if ( c == 0 ) {

                               return "1.25em";
                           } else {

                               return "-0.75em";
                           }
                       } )
                .attr( "class", "dot-text" )
                .text( function ( d ) {
                           var tMin = d.conditions[0].minTemperature.toFixed( 1 ),
                               tMax = d.conditions[0].maxTemperature.toFixed( 1 )
                           if ( c == 0 ) {
                               return tMin;
                           } else {
                               return tMax;
                           }

                       } );

        }


    } ); // close .get(json)
} ); // close controller
