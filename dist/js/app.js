/**
 * Created by iankleinfeld on 10/2/2014.
 */


var app = angular.module( 'scApp', ["ngResource"] );

app.location = {
    "latitude" : 7.921330,
    "longitude": -1.204386
};

app.fx = {
    'months'       : function ( m ) {
        var abbr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return abbr[m.getMonth()];
    },
    'days'         : function ( d ) {
        var dayName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        return dayName[d.getDay()];
    },
    'whatShortDate': function ( theDate ) {
        var formattedDate = new Date( theDate );
        return formattedDate.getDate() + ' ' + app.fx.months( formattedDate );

    },
    "cloneD3": function(selection, newID){
        console.log( selection.length );
        var attr = selection.node().attributes;
        var length = attr.length;
        var node_name = selection.property("nodeName");
        var parent = d3.select(selection.node().parentNode);
        var cloned = parent.append(node_name)
            .attr("id", selection.attr("id") + newID);
        for (var j = 1; j < length; j++) { // Start at '1' to skip the id attribute
            cloned.attr(attr[j].name,attr[j].value);
        }
        return cloned;
    },
    "svg":{
        "translateMe": function(x,y){
            return "translate(" + x + "," + y + ")";
        }
    }


};

